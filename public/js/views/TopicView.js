define(['jquery', 'underscore', 'backbone', 'models/Topic'], function($, _, Backbone, Topic) {
	var TopicView = Backbone.View.extend({
		tagName: 'li'
		, template: _.template($('#topic-item-template').html())
		, events: {
			
		}
		, initialize: function () {
			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'destroy', this.remove);
		}
		, render: function () {
			this.$el.html(this.template(this.model.toJSON()));

			this.$('.gird').each(function(index, item) {
				var fade = $(item).siblings('.gird').find('.fade');
				var btns = $(item).find('.btn-box');
				$(item).hover(function() {
					fade.show();
					btns.show();
				}, function() {
					fade.hide();
					btns.hide();
				});
			});

			return this;
		}
		, clear: function () {
			this.model.destroy();
		}

	});

	return TopicView;
});