define(['jquery', 'underscore', 'backbone', 'models/Topic'], function($, _, Backbone, Topic) {
	var MyTopicView = Backbone.View.extend({
		tagName: 'li'
		, template: _.template($('#my-topic-item-template').html())
		, events: {
			
		}
		, initialize: function () {
			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'destroy', this.remove);
		}
		, render: function () {
			this.$el.html(this.template(this.model.toJSON()));
			return this;
		}
		, clear: function () {
			this.model.destroy();
		}

	});

	return MyTopicView;
});