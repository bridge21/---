define(
	['jquery', 'underscore', 'backbone', 'models/Topic', 'models/Topics', 'views/TopicView', 'flexslider' ]
	, function($, _, Backbone, Topic, Topics, TopicView) {
		var topics = new Topics;

		var TopicsView = Backbone.View.extend({
			el:  $('#all-topics')
			, events: {
				
			}
			, initialize: function () {
				this.listenTo(topics, 'add', this.addOne);
				this.listenTo(topics, 'reset', this.addAll);
				this.listenTo(topics, 'all', this.render);

				topics.fetch();
				// topics.fetch({success: function(res) {
				// 	topics.reset();
				// 	console.log(res.length);
				//   	 _.each(res, function(model) {
				//      		topics.create(model);
				//    	});
				//  }});
			}
			, render: function () {
				return this;
			}
			, addOne: function (model) {
				var view = new TopicView({model: model});
				this.$el.append(view.render().el);
			}
			, addAll: function () {
				topics.each(this.addOne, this);
				
				$('.flexslider').flexslider({
					animation: 'fade',
					slideshow: true, 
					pauseOnHover: true,
					slideshowSpeed: 5000,
				});
			}
		});

        	return TopicsView;          
});