define(
	['jquery', 'underscore', 'backbone', 'models/Todo', 'models/TodoList', 'views/TodoView']
	, function($, _, Backbone, Todo, TodoList, TodoView) {
		var Todos = new TodoList;

	        	var AppView = Backbone.View.extend({
			el:  $('#todoapp')
			, statusTemplate: _.template($('#footer-template').html())
			, events: {
				'keypress #new-todo': 'createOnEnter'
				, 'click #clear-completed': 'clearCompleted'
				, 'click #toggle-all': 'toggleAllCompleted'
			}
			, initialize: function () {
				this.input = this.$('#new-todo');
				this.allCheckBox = this.$('#toggle-all')[0];

				this.listenTo(Todos, 'add', this.addOne);
				this.listenTo(Todos, 'reset', this.addAll);
				this.listenTo(Todos, 'all', this.render);

				this.footer = this.$('#footer');
				this.main = this.$('#main');

				Todos.fetch();
			}
			, render: function () {
				var done  = Todos.done().length;
				var remaining = Todos.remaining().length;

				if (Todos.length) {
					this.main.show();
					this.footer.show();
					this.footer.html(this.statusTemplate({done: done, remaining: remaining}));
				}
				else {
					this.main.hide();
					this.footer.hide();
				}

				// this.allCheckBox.checked = !remaining;
				return this;
			}
			, addOne: function (model) {
				var view = new TodoView({model: model});
				this.$('#todo-list').append(view.render().el);
			}
			, addAll: function () {
				Todos.each(this.addOne, this);
			}
			, createOnEnter: function (e) {
				if (e.keyCode != 13 || !this.input.val()) 
					return;
				Todos.create({title: this.input.val()});
				this.input.val('');
			}
			, clearCompleted: function () {
				_.invoke(Todos.done(), 'destrory');
			}
			, toggleAllCompleted: function () {
				var value = this.allCheckBox.checked;
				Todos.each(function(todo){
					todo.save({done: value});
				});
			}
		});

        	return AppView;          
});