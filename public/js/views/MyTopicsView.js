define(
	['jquery', 'underscore', 'backbone', 'models/Topic', 'models/MyTopics', 'views/MyTopicView']
	, function($, _, Backbone, Topic, MyTopics, MyTopicView) {
		var myTopics = new MyTopics;

		var TopicsView = Backbone.View.extend({
			el:  $('#my')
			, events: {
				
			}
			, initialize: function () {
				this.listenTo(myTopics, 'add', this.addOne);
				this.listenTo(myTopics, 'reset', this.addAll);
				this.listenTo(myTopics, 'all', this.render);

				// myTopics.fetch();
				myTopics.reset(mock_my_topics);
			}
			, render: function () {
				return this;
			}
			, addOne: function (model) {
				var view = new MyTopicView({model: model});
				this.$el.append(view.render().el);
			}
			, addAll: function () {
				myTopics.each(this.addOne, this);
			}
		});

        	return TopicsView;          
});