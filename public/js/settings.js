var ALL_TOPICS_INTERFACE = 'topic/all';

require.config({
      baseUrl: '../js/'
    	, shim: {
                'underscore': {
        			deps: ['jquery']
        			, exports: '_'
      		}
      		, 'backbone': {
        			deps: ['underscore', 'jquery']
        			, exports: 'Backbone'
      		}
               , 'backbone-cache': {
                    deps: ['underscore', 'jquery', 'backbone']
                    , exports: 'backbone-cache'
                }
                , 'backbone-localstorage': {
                    deps: ['underscore', 'jquery', 'backbone']
                    , exports: 'backbone-localstorage'
                }
                , 'flexslider': {
                    deps:  ['jquery']
                }
    	}
     , paths: {
      		'jquery': 'libs/jquery'
      		, 'underscore': 'libs/underscore'
      		, 'backbone': 'libs/backbone'
                , 'backbone-cache': 'libs/backbone.fetch-cache.min'
                , 'backbone-localstorage': 'libs/backbone.localstorage'
                , 'flexslider': 'libs/jquery.flexslider'
	}
});



require(['underscore'], function(_) {
    _.templateSettings = {
      /*evaluate:    /\{\{(.+?)\}\}/g,
      interpolate: /\{\{=(.+?)\}\}/g
      escape:/\{\{-(.+?)\}\}/g*/
      evaluate:/\[%([\s\S]+?)%\]/g
      ,interpolate:/\[%=([\s\S]+?)%\]/g
      ,escape:/\[%-([\s\S]+?)%\]/g
    }

});