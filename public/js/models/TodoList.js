define(['underscore', 'backbone', 'backbone-localstorage', 'models/Todo'], function(_, Backbone, Storage, Todo) {
	var TodoList = Backbone.Collection.extend({
		model: Todo
		, localStorage: new Backbone.LocalStorage('todos')
		, done: function () {
			return this.filter(function(item) {
				return item.get('done');
			});
		}
		, remaining: function () {
			return _.without(this, this.done());
		}
	});
	
	return TodoList;
});