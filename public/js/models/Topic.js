define(['underscore', 'backbone'], function(_, Backbone) {
	var Topic = Backbone.Model.extend({
		defaults: function() {
			return {
				counts: 0
			}
		}
		, initialize: function() {
			this.set('items', _.sortBy(this.get('items'), function(item) {
				return -item.yes;
			}));  

			this.set('counts',  _.reduce(this.get('items'), function(a, b) {
				return a + b.yes + b.no;
			}, 0));  

			// console.log(this);
		}
	});
	
	return Topic;
});