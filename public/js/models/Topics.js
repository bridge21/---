define(['underscore', 'backbone', 'backbone-localstorage', 'models/Topic'], function(_, Backbone, Storage, Topic) {
	var Topics = Backbone.Collection.extend({
		model: Topic
		, url: ALL_TOPICS_INTERFACE
	});
	return Topics;
});