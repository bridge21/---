define(['underscore', 'backbone'], function(_, Backbone) {
	var Todo = Backbone.Model.extend({
		defaults: function () {
			return {
				title: "woshinibaba"
				, done: false
			}
		}
		, initialiaze: function() {
			if (!this.get('title'))
				this.set('title', this.defaults.title);
		}
		, toggle: function () {
			this.save({done: !this.get('done')});
		}
	});

	return Todo;
});