define(['underscore', 'backbone', 'backbone-localstorage', 'models/Topic'], function(_, Backbone, Storage, Topic) {
	var MyTopics = Backbone.Collection.extend({
		model: Topic
		// , url: ALL_TOPICS_INTERFACE
		, initialize: function() {
		}
	});

	return MyTopics;
});