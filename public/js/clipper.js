(function($) {
        var USER_DATA_INFTERFACE = 'http://t.bridge21.info/picker/get?callback=?';
        var USER_SAVE_INFTERFACE = 'http://t.bridge21.info/picker/add?callback=?';
        var USER_VOTE_INFTERFACE = 'http://t.bridge21.info/topic/create_by_picker?callback=?';

        var blacklist = [/facebook\.com$/,/google\.com$/,/flickr\.com$/];
        
        var user_sel_items = [];
        var images = [];
        var imageHash = {};
        var foundOGImage = false;

        var minImageDimension = 300;
        var squareWidth = 200;
        var squareHeight = 200;
        var imageBoxWidth = 120;
        var imageBoxHeight = 120;
        var imageBoxBorder = 4;
        var maxImageRatio = 3;

        var docDomain = document.domain;
        var saveScrollHeight = window.pageYOffset;
        var bodyHeightStyle = Math.max(document.body.scrollHeight, document.body.offsetHeight) + 'px';

        var bgElem = $('<div id="jmt-bg" class="jmt-cover"/>');
        var pickerElem = $('<div id="jmt-picker" class="jmt-cover"/>');
        var imageSquare = $('<div class="jmt-image-square"/>');
        var thumbWrapper = $('<div class="jmt-image-wrapper">');
        
        var imageBox;
        var loading;
        var closeBtn;
        var saveBtn;
        var voteBtn;
        var imageContainer;

        var removeBookmarkletDom = function() {
            var ids = ['jmt-picker', 'jmt-shim', 'jmt-bg'];
            for (var i=0; i < ids.length; i++) {
                $('#' + ids[i]).remove();
            }
            window.scroll(0, saveScrollHeight);
        }

        var closeAll = function() {
            pickerElem.css('opacity', 0);
            bgElem.css('opacity', 0);

            setTimeout(removeBookmarkletDom, 150);
        }

        var getUserSelected = function() {
            $.getJSON(USER_DATA_INFTERFACE, function(data) {
                loading.hide();
                user_sel_items = data
                updateImageBox();
            });  
        }

        var save = function() {
            if(user_sel_items && user_sel_items.length > 0)
                $.getJSON(USER_SAVE_INFTERFACE,{pics:user_sel_items},function(data) {
                    console.log(data);
                });
        }

        var vote = function() {
            if(user_sel_items && user_sel_items.length > 0)
            $.getJSON(USER_VOTE_INFTERFACE,{pics:user_sel_items},function(data) {
                console.log(data);
            });
        }

        var preventScrolling = function(event) {
            var delta = 0;
            if(event.wheelDelta) {
                delta = event.wheelDelta/120;
            } else if (event.detail) {
                delta = -event.detail/3;
            }
            if((this.scrollTop === (this.scrollHeight - this.offsetHeight) && delta < 0) || (this.scrollTop === 0 && delta > 0)) {
                if(event.preventDefault) {
                    event.preventDefault();
                }
                event.returnValue = false;
            }
        }

        var generateImage = function(src, maxWidth, maxHeight) {
            var img = new Image();

            img.style.opacity = '0';
            img.onload =function() {
                var width = this.width;
                var height = this.height;
                if(width < height) {
                    var ratio = maxHeight / height;
                } else {
                    var ratio = maxWidth / width;
                }
                if(width < maxWidth && height < maxHeight) {
                    var ratio = 1;
                }

                var newWidth = Math.floor(width * ratio);
                var newHeight = Math.floor(height * ratio);
                this.width = newWidth;
                this.height = newHeight;
                this.style.left = Math.floor((maxWidth - newWidth)/2) + 'px';
                this.style.top = Math.floor((maxHeight - newHeight)/2) + 'px';
                this.style.opacity = '1';
            };

            img.src = src;

            return img;
        }

        var updateImageBox = function(){
            var length = user_sel_items.length;
            
            $('.jmt-images-container').empty();
            $('.jmt-images-container').width((imageBoxWidth + imageBoxBorder)* length);

            $.each(user_sel_items, function(index, item) {
                var _item = item;
                var img = generateImage(_item.image, imageBoxWidth, imageBoxHeight);

                 $('<div class="box active"/>')
                    .appendTo(imageBox)
                    .append(img)
                    .on('click', function() {
                        if ($(this).hasClass('active')) {
                            $(this).removeClass('active');
                            delPicFromSelectedBox(_item.image, _item.source);
                        } else {
                            $(this).addClass('active');
                            addPicToSelectedBox(_item.image, _item.source);
                        }
                    });
            });
       }

        var addPicToSelectedBox = function(image,source){
            user_sel_items.push({
                image:image
                ,source:source
            });

            updateImageBox();
        };

        var delPicFromSelectedBox = function(image,source){
            for(var i = 0; i<user_sel_items.length;i++)
                if(user_sel_items[i].image == image && user_sel_items[i].source == source){
                    user_sel_items.splice(i,1);
                    break;
                }

            updateImageBox();
        };

        var addImageToPicker = function(imageSrc) {
            //remove gif
            var pos = imageSrc.lastIndexOf('.');
            var ext = imageSrc.substring(pos, imageSrc.length);
            if (ext.toLowerCase() === '.gif')
                return;
            
            var img = generateImage(imageSrc, squareWidth, squareHeight);
            
            imageSquare.clone()
                .append(thumbWrapper
                .clone()
                .append(img))
                .appendTo(imageContainer)
                .on('click', function() {
                    // console.log($(this));
                    var image = $(this).find('img').attr('src');
                    var source = document.location.href;

                    if ($(this).hasClass('active')) {
                        $(this).removeClass('active');
                        delPicFromSelectedBox(image, source);
                    } else {
                        $(this).addClass('active');
                        addPicToSelectedBox(image, source);
                    }
                });

            // if(img.width > 0 || img.height > 0) {
            //     img.onload();
            //     img.onload = null;
            // }
        }



        for (var i=0; i < blacklist.length; i++)
        {
            var pattern = blacklist[i];
            if(docDomain.match(pattern)){
                alert('来自'+docDomain+' 的图片不可以收集到姐妹淘');
                return;
            }
        }

        removeBookmarkletDom();
        window.scroll(0, 0);

        bgElem.css({height: bodyHeightStyle})
            .appendTo('body');

        pickerElem.html(
                '<div id="jmt-picker-header">' +
                    '<a id="jmt-picker-close">&times;</a>' +
                    '<div id="jmt-logo"></div>' +
                    '<div id="jmt-picked">' +
                        '<div id="jmt-loading"></div>' +
                        '<div id="jmt-images-box">' +
                            '<div class="jmt-images-container">' +
                            '</div>' +
                        '</div>' +
                        '<div class="jmt-button-boxs">' +
                            '<a id="jmt-btn-save">保存已选</a><a id="jmt-btn-vote">发起投票</a>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
                '<div id="jmt-picker-container"></div>'
            )
            .appendTo('body');

        loading = $(pickerElem.find('#jmt-loading'));

        imageBox = $(pickerElem.find('#jmt-images-box .jmt-images-container'));

        closeBtn =
            $(pickerElem.find('#jmt-picker-close'))
                .on('click', closeAll);
        
        saveBtn = 
            $(pickerElem.find('#jmt-btn-save'))
                .on('click', save);

        voteBtn = 
            $(pickerElem.find('#jmt-btn-vote'))
                .on('click', vote);

        imageContainer =
            $(pickerElem.find('#jmt-picker-container'))
                .on('DOMMouseScroll', preventScrolling)
                .on('mousewheel', preventScrolling);

        $(document).on('keydown', function(e) {
            if(e.keyCode == 27) { closeAll(); }
        });


        //web site images
        for (var i=0; i < document.images.length; i++) {
            var img = document.images[i];
            if(!img.src.replace(/\s/g, '')) {
                continue;
            }
            var w = img.width;
            var h = img.height;
            var longer = Math.max(w,h);
            var shorter = Math.min(w,h);
            if(longer >= minImageDimension && (longer/shorter) <= maxImageRatio && !imageHash[img.src]) {
                imageHash[img.src] = true;
                images.push(img);
            }
        }

        var metaTags = document.getElementsByTagName('meta');
        for (var i=0; i < metaTags.length; i++) {
            if(metaTags[i].getAttribute('property') == 'og:image') {
                var src = metaTags[i].getAttribute('content');
                if(!imageHash[src]) {
                    addImageToPicker(src);
                    foundOGImage = true;
                }
                break;
            }
        };

        if(!images.length && !foundOGImage) {
            removeBookmarkletDom();
            alert('没有找到合适的图片!')
        }

        images.sort(function(a,b) {
            return (b.width * b.height) - (a.width * a.height);
        });

        for (var i=0; i < images.length; i++) {
            addImageToPicker(images[i].src);
        }

        //get user selected images from server
        getUserSelected();
})(jQuery);





