require(['jquery', 'models/Topics', 'views/TopicsView'
	, 'models/MyTopics', 'views/MyTopicsView'], 
	function($, Topics, TopicsView, MyTopics, MyTopicsView) {
	$(function() {
		var personal = $('#btn-personal');
		var side = $('.side');
		var container = $('.container');
		var footer = $('#footer');
		var main = $('#main');
		var vote = $('#btn-vote');
		var voteModal = $('#vote-modal');

		//resize
		main.height($(window).height() - footer.height());

		$(window).resize(function() {
			main.height($(window).height() - footer.height());
			if (side.css('display') == 'block')
				container.width($(document).width() - side.width() > 0?
						$(document).width() - side.width(): 574);
		});


		//modal
		vote.on('click', function() {
			$('body').append('<div id="overlay" class="fade"/>');
			$('#overlay').show();

			voteModal.show();
		});

		var close = $('.modal .close');
		close.on('click', function() {
			$('#overlay').remove();
			$('.modal').hide();
		});

		personal.on('click', function() {
			if (side.css('display') == 'none') {
				side.show();

				container.animate({
					width: $(document).width() - 400 > 0?
						$(document).width() - 400: 574
				}, 200);
			} else {
				container.animate({
					width: '100%'
				}, 200, function() {
					side.hide();
				});
			}
		});

		// side
		var side = $('#side');
		var votes = side.find('.votes');
		var nav = side.find('ul.nav li a');

		nav.each(function(index, item) {
			var _item = $(item);
			var _index = index;
			_item.on('click', function() {
				nav.removeClass('active');
				_item.addClass('active');

				$(votes).hide();
				$(votes[_index]).show();
			});
		});

		//backbone  data
		// var topics = new Topics;
		var topicsView = new TopicsView;
		// topics.fetch();
		// topics.reset(mock_topics);

		// var myTopics = new MyTopics;
		var myTopicsView = new MyTopicsView;
		// myTopics.reset(mock_my_topics);

		
	});
});

