# -*- encoding : utf-8 -*-
require 'spec_helper'

describe User do

  before(:each) do
    @user = User.new(:name => "warren", :type => "weibo", :score => 0)
  end

  it "is not valid without a name" do
    @user.name = nil
    @user.should_not be_valid
  end

  it "is not valid without a type" do
    @user.type = nil
    @user.should_not be_valid
  end

  it "is not valid without a score" do
    @user.score = nil
    @user.should_not be_valid
  end

  it "is valid with valid attributes" do 
    @user.should be_valid
  end
 
end
