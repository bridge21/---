# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Topic do

  before(:each) do
    owner = User.create(:name => "warren", :type => "weibo", :score => 0)
    @topic = Topic.new(:title => "Please help me choose", :type => 1, :user => owner)
  end

  it "is not valid without a title" do
    @topic.title = nil
    @topic.should_not be_valid
  end

  it "is not valid without a type" do
    @topic.type = nil
    @topic.should_not be_valid
  end

  it "is not valid without an owner" do
    @topic.user = nil
    @topic.should_not be_valid
  end

  it "is valid with valid attributes" do 
    @topic.should be_valid
  end

  context "when the user is logged in" do
    before(:each) do
      @david = User.create(:name => "david", :type => "weibo", :score => 0)
    end

    it "add a topic" do
      @topic.user = @david
      @topic.save
      Topic.should have(1).record
    end

  end
  

end
