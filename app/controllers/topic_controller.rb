# -*- encoding : utf-8 -*-
class TopicController < ApplicationController

  before_filter :require_login

  def index
    #session[:init] = true
    @user = session["logged"]? User.find(session["id"]) : nil
  end

  def all
    @topics = Topic.where("ttype=?",1) #ttype = 1 pubilc
    render json: @topics.map{|t| {
      :id => t.id, 
      :create_time => t.created_at, 
      :user => {:name => t.user.name, :avatar => t.user.avatar},
      :items => t.items.map{|i| {:item_id=>i.id, :image=>i.images.first.image, :url=>i.images.first.url}},
    }}
  end

  def create
    user = User.find(session["id"])
    @topic = Topic.new({:ttype=>1})
    @topic.user = user
    if params[:pics]
      params[:pics].split('$').each do |url|
        temp_item = create_item(url)
        if temp_item
          @topic.items.push(temp_item)
        end
      end
      code = @topic.save
    end
    if code
      render json: {"status" => 1}
    else
      render json: {"status" => -1}
    end
  end

  def create_by_picker
    user = User.find(session["id"])
    @topic = Topic.new({:ttype=>1})
    @topic.user = user
    pics = params[:pics]
    user.temppics.delete_all #delete all pics
    pics.each do |k,pic|
      temp_item = create_item(pic["image"],pic["source"])
      if temp_item
          @topic.items.push(temp_item)
      end
    end
    if @topic.save
      render json: {"status" => 1}, :callback=>params[:callback] 
    else
      render json: {"status" => -1}, :callback=>params[:callback] 
    end
  end

  # 投票
  def vote #params item_id
    item = Item.find(params[:item_id])
    user = User.find(session["id"])
    if item && user
      if user.voted_items.map{|vi| vi.id}.include? item.id
        code = -1 #
      else
        result = Result.new({
          :item => item,
          :user => user,
          :score => params[:value]
        })
        result.save
        params[:value] == 1 ? item.yes += 1 : item.no += 1
        item.save
        code = 1 #成功
      end
    end
    render json: {"status" => code}
  end

  #一个topic的所有投票
  def votes #params topic_id
    topic = Topic.find(params[:topic_id])
    render json: topic.votes.map{|v| {
      :create_time => v.created_at,
      :item_id => v.item.id,
      :user => {:name=>v.user.name, :avatar=>v.user.avatar},
      :value => v.score
    }}
  end

  private
    def create_item(image_url,source)
      item = Item.new
      image = upload_to_local(image_url,source)
      if !image.nil?
        item.images.push(image)
        item.save
        return item
      else
        return nil
      end
    end

    def upload_to_local(image,source)
      # save files in local server
      begin
        pic = MiniMagick::Image.open(image)
        pic.format "jpg"
        image = Image.create!
        pic.write("public/uploads/#{image.id.to_s}.jpg")
        image.image = "uploads/#{image.id.to_s}.jpg"
        image.url = source
        image.save
        return image
      rescue Exception => e
        puts "Error:#{e}"
        return nil          
      end
    end

    # def parse_url(urls)
    #   urls.split('$')
    # end

    def upload_pics(urls)
      upload_to_local(urls)
    end

    def upload_to_qiniu(urls)
      # todo callback
      upload_token = Qiniu::RS.generate_upload_token :scope              => "jiemeitao"
                                                  # ,:expires_in         => expires_in_seconds
                                                  # ,:callback_url       => callback_url
                                                  # ,:callback_body_type => callback_body_type
                                                  # ,:customer           => end_user_id
                                                  # ,:escape             => allow_upload_callback_api
                                                  # ,:async_options      => async_callback_api_commands
                                                  # ,:return_body        => custom_response_body
      urls.each do |url|
        begin
          pic = MiniMagick::Image.open(url)
          pic.format "gif"
          image = Image.create
          # todo image's relationship
          pic.write("upload/#{image.id.to_s}.gif")
          Qiniu::RS.upload_file :uptoken            => upload_token,
                                :file               => "upload/#{image.id.to_s}.gif",
                                :bucket             => "jiemeitao",
                                :key                => image.id.to_s
                                # ,:mime_type          => file_mime_type
                                # ,:note               => some_notes
                                # ,:callback_params    => callback_params
                                # ,:enable_crc32_check => fals
                                # ,:rotate             => auto_rotate
          File.delete("upload/#{image.id.to_s}.gif")
        rescue Qiniu::UploadFailedError => err
          puts "#{err}"
          return -1
        rescue Exception => e
          puts "#{e}"
          return -1
        end
      end
      return 1
    end

    # for authorization
    def require_login
      unless is_authed?
        redirect_to "/login"
      end
    end

    def is_authed?
      return false if session["logged"].nil?
      client = Weibo2::Client.from_hash({"token"=>session["token"], "expires_at"=>session["expires_at"]})
      return client.is_authorized?
    end

    def is_expired?
    end

    def error_beautify(code)
      error_description = Code::WEIBO_ERROR_INFO[code.to_s]
      error_description.nil?? "未知错误(错误代码：#{code})" : "#{error_description}"
    end


end
