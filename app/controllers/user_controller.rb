# -*- encoding : utf-8 -*-
# -*- coding: utf-8 -*-
class UserController < ApplicationController

  before_filter :require_login, :except => [:login,:auth,:callback]

  def login  
  end
  
  def auth
    client = Weibo2::Client.new
    authurl = client.auth_code.authorize_url
    redirect_to authurl
  end

  def callback
    authcode = params[:code]
    begin
      client = Weibo2::Client.from_code(authcode)
      uid = client.account.get_uid.parsed["uid"]
      profile = client.users.show({:uid => uid}).parsed
      p profile

      user = User.find_by_uid(uid)
      if user.nil?
        user = User.create!({:uid=>uid, :name=>profile["screen_name"], :utype=>1, :role=>"ord", :avatar=>profile["profile_image_url"]})
      end

      session["logged"] = true
      session["id"] = user.id
      session["uid"] = user.uid
      session["type"] = user.utype
      session["token"] = client.token.token
      session["expires_at"] = client.token.expires_at 
      session["img_url"] = profile["profile_image_url"]
      session["screen_name"] = profile["screen_name"]
      #p session
    rescue Weibo2::Error => ex
      @notice = error_beautify(ex)
    rescue => ex
      @notice = "unknow exception: #{ex.message}"
    end
    redirect_to "/"
  end

  def logout
    reset_session
    begin
      Weibo2::Client.from_hash({"token"=>session["token"], "expires_at"=>session["expires_at"]}).account.logout
    rescue Weibo2::Error => ex
      puts "ERROR:#{Code::WEIBO_ERROR_INFO[ex.code.to_s]}"
    rescue => ex
      puts "unknown exception: #{ex.message}"
    end
    redirect_to "/login"
  end

  #我发起的投票
	def topics
    user = User.find(session['id'])
		@topics = user.topics.map{|t| {
      :topic_id=>t.id, 
      :create_time=>t.created_at, 
      :items => t.items.map{|i| {:item_id=>i.id, :image=>i.images.first.image, :url=>i.images.first.url}},
      :par_count=>t.par_count
    }}
		render json: @topics
	end

  #我参与的投票
  def votes
    user = User.find(session["id"])
    render json: user.results.map{|r| {
      :topic_id => r.item.topic.id,
      :create_time => r.created_at,
      :item_id => r.item.id,
      :items => r.item.topic.items.map{|i| {:item_id=>i.id, :image=>i.images.first.image, :url=>i.images.first.url, :yes=>i.yes, :no=>i.no}},
      :user => {:name=>user.name, :avatar=>user.avatar},
      :par_count => r.item.topic.par_count
    }}
  end

	def info  #params[:id] : user info
		@user = User.find(params[:id])
    render json: @user.to_json(:only => [:id,:name,:avatar])
	end

	def create #create user
	end

  private
    def require_login
      unless is_authed?
        redirect_to "/login"
      end
    end

    def is_authed?
      return false if session["logged"].nil?
      client = Weibo2::Client.from_hash({"token"=>session["token"], "expires_at"=>session["expires_at"]})
      return client.is_authorized?
    end

    def error_beautify(code)
      error_description = Code::WEIBO_ERROR_INFO[code.to_s]
      error_description.nil?? "未知错误(错误代码：#{code})" : "#{error_description}"
    end

end
