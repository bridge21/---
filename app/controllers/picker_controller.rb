# -*- encoding : utf-8 -*-
# -*- coding: utf-8 -*-
#用于和采集器交互
class PickerController < ApplicationController
	def index
     end

	def get
		if session["logged"] #登录成功
			user =User.find_by_id(session["id"])
		     @temppics = user.temppics
		     render :json =>@temppics, :callback=>params[:callback]
		else
			render json: {"status" => -1}, :callback=>params[:callback] #尚未登录
		end
	end
     
     def add
     	if session["logged"] #登录成功
	     	pics = params[:pics]
	     	user =User.find_by_id(session["id"])
	     	user.temppics.delete_all #delete all pics
	     	pics.each do |k,pic|
	     		single(pic["image"],pic["source"], user)
	     	end
	     	render json: {"status" => 1},:callback=>params[:callback]  #成功
	     else
	     	render json: {"status" => -1},:callback=>params[:callback]#尚未登录
	     end
     end

	def single(image,source,user)
		temppic = Temppic.where("user_id = ? and image = ? ",user.id, image)
		if temppic.nil? #存在相同来源的图片
			return -1
		else
			temppic = Temppic.new({:image=>image,:source=>source})
			temppic.user = user
			temppic.save
			return 1
		end
	end
end
