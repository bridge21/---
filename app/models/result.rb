# -*- encoding : utf-8 -*-
class Result < ActiveRecord::Base
  attr_accessible :item, :user, :score

	belongs_to :item
	belongs_to :user
end
