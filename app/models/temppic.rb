# -*- encoding : utf-8 -*-
class Temppic < ActiveRecord::Base
  attr_accessible :image, :source
  belongs_to :user
end
