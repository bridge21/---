# -*- encoding : utf-8 -*-
class Topic < ActiveRecord::Base
  attr_accessible :title, :content, :ttype, :user, :image

  validates_presence_of :ttype, :user

  has_many :comments,:dependent => :destroy
  has_many :items, :dependent => :destroy
  has_many :usertopics, :dependent => :destroy   
  belongs_to :user
  has_many :invited_users, #私密邀请
  		   :through => :usertopics,
		   :source => :user
  #attr_accessor :image  #first image
  
  def image       
    @image || items.first.images.first.image       
  end

  def images
    images = []
    self.items.each do |item|
      images  << item.images.first.image
    end 
    images
  end

  def voted_image(current_user)
    my_image = []
    current_user.voted_items.each do |vitem|
      my_image << vitem.images.first.image if item.topic == self
    end
    my_image
  end

  def votes
    votes = []
    self.items.each do |item|
      votes += item.results
    end
    votes
  end

  def par_count
    par_count = 0
    self.items.each do |item|
      par_count += item.voted_users.count
    end
    par_count
  end

end
