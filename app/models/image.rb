# -*- encoding : utf-8 -*-
class Image < ActiveRecord::Base
  attr_accessible :image, :url
	belongs_to :item
end
