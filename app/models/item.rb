# -*- encoding : utf-8 -*-
class Item < ActiveRecord::Base
  belongs_to :topic
  has_many :images
  has_many :results
  has_many :voted_users, #私密邀请
  		   :through => :results,
		   :source => :user 
end
