# -*- encoding : utf-8 -*-
class User < ActiveRecord::Base
  attr_accessible :uid, :name, :role, :email, :utype, :description, :score, :avatar

  validates_presence_of :uid, :role, :name, :utype, :score

  has_many :topics
  has_many :results
  has_many :voted_items, :through => :results, :source => :item
  has_many :comments
  has_many :usertopics,:dependent => :destroy
  has_many :invited_topics, :through => :usertopics, :source => :topic 
  has_many :temppics
end
