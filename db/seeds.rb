# -*- encoding : utf-8 -*-
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user1 = User.new
user1.uid = 1974877341
user1.name = 'Bridge21_warren'
user1.utype = 1
user1.score = 0
user1.role = 'ord'
user1.avatar = "http://tp2.sinaimg.cn/1974877341/50/5602913998/1"
user1.save

user2 = User.new
user2.uid = 1642675220
user2.name = 'Pp歪脖派大星'
user2.utype = 1
user2.score = 0
user2.role = 'ord'
user2.avatar = "http://tp1.sinaimg.cn/1642675220/180/40012506763/1"
user2.save

user3 = User.new
user3.uid = 2175035757
user3.name = '皮蛋瘦肉粥_华磊'
user3.utype = 1
user3.score = 0
user3.role = 'ord'
user3.avatar = "http://tp2.sinaimg.cn/2175035757/180/5602822492/1"
user3.save


user4 = User.new
user4.uid = 2128050865
user4.name = '尼亚的鬼屋'
user4.utype = 1
user4.score = 0
user4.role = 'ord'
user4.avatar = "http://tp2.sinaimg.cn/2128050865/180/40014924988/1"
user4.save



image1 = Image.create({image: 'http://tbphoto3.bababian.com/upload7/wbandlwbandl/201303/001121641378.jpg', url: 'http://baidu.com' })
image2 = Image.create({image: 'http://tbphoto2.bababian.com/upload3/zouwen853888919/3001/3_m.jpg', url: 'http://baidu.com' })
image3 = Image.create({image: 'http://img04.taobaocdn.com/imgextra/i4/22438470/T2pk_gXm4aXXXXXXXX_!!22438470.jpg', url: 'http://baidu.com' })
image4 = Image.create({image: 'http://img02.taobaocdn.com/imgextra/i2/394695430/T2Xa6mXdNaXXXXXXXX_!!394695430.jpg', url: 'http://baidu.com' })
image5 = Image.create({image: 'http://img04.taobaocdn.com/imgextra/i4/82576001/T2NpedXhlcXXXXXXXX_!!82576001.jpg', url: 'http://baidu.com' })
image6 = Image.create({image: 'http://img03.taobaocdn.com/imgextra/i3/720277821/T2kJzgXl4XXXXXXXXX_!!720277821.jpg', url: 'http://baidu.com' })

item1 = Item.create({  })
item1.images.push(image1)
item1.save
item2 = Item.create({  })
item2.images.push(image2)
item2.save
item3 = Item.create({  })
item3.images.push(image3)
item3.save
item4 = Item.create({  })
item4.images.push(image4)
item4.save
item5 = Item.create({  })
item5.images.push(image5)
item5.save
item6 = Item.create({  })
item6.images.push(image6)
item6.save

topic1 = Topic.new
topic1.user = user1
topic1.ttype = 1
topic1.items.push(item1)
topic1.save
topic2 = Topic.new
topic2.user = user1
topic2.ttype = 1
topic2.items.push(item2)
topic2.save
topic3 = Topic.new
topic3.user = user2
topic3.ttype = 1
topic3.items.push(item3)
topic3.save
topic4 = Topic.new
topic4.user = user1
topic4.ttype = 1
topic4.items.push(item4)
topic4.save
topic5 = Topic.new
topic5.user = user3
topic5.ttype = 1
topic5.items.push(item5)
topic5.save
topic6 = Topic.new
topic6.user = user1
topic6.ttype = 1
topic6.items.push(item6)
topic6.save
