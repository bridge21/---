# -*- encoding : utf-8 -*-
class CreateJiemeitaoTable < ActiveRecord::Migration
 def up
  create_table :topics do |t|  
    t.string :title
    t.text :content
    t.integer :ttype #private or public
    t.integer :comment_count, :default => 0
    t.references :user
    t.timestamps
  end  
	  
  create_table :comments do |t|  
    t.text :content
    t.integer :like, :default => 0
    t.integer :unlike, :default => 0
    t.timestamps
    t.references :topic
    t.references :user
  end  

  create_table :users do |t|  
    t.string :name
    t.string :role
    t.string :email
    t.integer :uid, :limit => 8
    t.integer :utype #weibo:1 qq:2
    t.string :description
    t.string :follow
    t.string :friend
    t.integer :score, :default => 0
    t.string :avatar
    t.timestamps
  end 

  create_table :results do |t|
    t.references :item
    t.references :user
    t.integer :score, :default => 0 #-1投反对 1 投支持 0 没投
    t.timestamps
  end

  create_table :usertopics do |t|
    t.integer :user_id
    t.integer :topic_id
    t.timestamps
  end  

  create_table :items do |t|  
    t.string :content
    t.integer :yes, :default => 0
    t.integer :no, :default => 0
    t.references :topic
    t.timestamps
  end  

  
  create_table :images do |t| # one item has many image 
    t.string :image
    t.string :url
    t.references :item
    t.timestamps
  end 

 create_table :temppics do |t| #temp pic table
    t.string :image
    t.string :source
    t.references :user
    t.timestamps
 end 


 end

  def down
    drop_table :topics
    drop_table :users
    drop_table :results
    drop_table :comments
    drop_table :images
    drop_table :usertopics
    drop_table :items
    drop_table :temppics
  end
  
end  
